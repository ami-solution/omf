
Core profiler results
======================

Total objects               1704 objects
Parallelism                    1 thread
Total time                   2.0 seconds
  Core time                  1.9 seconds (93.7%)
    Compiler                 0.1 seconds (3.1%)
    Instances                0.0 seconds (0.0%)
    Random variables         0.0 seconds (0.0%)
    Schedules                0.0 seconds (0.0%)
    Loadshapes               0.0 seconds (0.0%)
    Enduses                  0.0 seconds (0.0%)
    Transforms               0.0 seconds (0.0%)
  Model time                 0.1 seconds/thread (6.3%)
Simulation time                0 days
Simulation speed              71 object.hours/second
Passes completed               3 passes
Time steps completed           2 timesteps
Convergence efficiency      1.50 passes/timestep
Read lock contention        0.0%
Write lock contention       0.0%
Average timestep            150 seconds/timestep
Simulation rate             150 x realtime


Model profiler results
======================

Class            Time (s) Time (%) msec/obj
---------------- -------- -------- --------
node               0.102     80.3%      0.2
voltdump           0.017     13.4%     17.0
overhead_line      0.002      1.6%      0.0
transformer        0.002      1.6%      0.0
triplex_node       0.002      1.6%      0.0
triplex_meter      0.002      1.6%      0.0
================ ======== ======== ========
Total              0.127    100.0%      0.1

